#coding=utf8

import dolfin
import glob
import numpy

import myPythonLibrary as mypy
import myVTKPythonLibrary as myvtk

import dolfin_dic as ddic

################################################################################

def get_coord_and_disp(
        working_folder,
        working_basename,
        ref_frame = 0,
        working_ext="vtu",
        disp_array_name="displacement",
        verbose=1):

    working_filenames = glob.glob(working_folder+"/"+working_basename+"_[0-9]*."+working_ext)
    assert (len(working_filenames) > 0), "There is no working file ("+working_folder+"/"+working_basename+"_[0-9]*."+working_ext+"). Aborting."

    working_zfill = len(working_filenames[0].rsplit("_",1)[-1].split(".")[0])
    if (verbose): print "working_zfill = " + str(working_zfill)

    n_frames = len(working_filenames)
    if (verbose): print "n_frames = " + str(n_frames)

    for k_frame in xrange(n_frames):

        print "k_frame = "+str(k_frame)

        if k_frame != ref_frame:

          mesh_filename = working_folder+"/"+working_basename+"_"+str(k_frame).zfill(working_zfill)+"."+working_ext
          print "mesh filename", mesh_filename
          mesh = myvtk.readUGrid(
              filename=mesh_filename,
              verbose=verbose)
          n_points = mesh.GetNumberOfPoints()
          print "mesh npoints", n_points

          nodes_displacement_file = open(working_folder+"/"+working_basename+"-node_displacement-"+str(k_frame).zfill(working_zfill)+".dat", "w")
          nodes_displacement_file.write("#n_node x y z dx dy dz\n")

          # assert (mesh.GetPoints().HasArray("Points"))
          # array_coord = mesh.GetPoints().GetArray("Points")
          assert (mesh.GetPointData().HasArray(disp_array_name))
          array_disp = mesh.GetPointData().GetArray(disp_array_name)

          for k_point in xrange(n_points):

            # strain_vs_radius_file.write(" ".join([str(val) for val in [k_frame, farray_rr.GetTuple1(k_cell)]+list(farray_strain.GetTuple(k_cell))]) + "\n")

            # nodes_displacement_file.write(str(k_point) + " " + str(mesh.GetPoint(k_point)[0]) + " " + str(mesh.GetPoint(k_point)[1]) + " " + str(mesh.GetPoint(k_point)[2]) + " " + str(array_disp.GetTuple(k_point)[0]) + " " + str(array_disp.GetTuple(k_point)[1]) + " " + str(array_disp.GetTuple(k_point)[2]) + "\n")

            nodes_displacement_file.write(" ".join([str(val) for val in [k_point]+list(mesh.GetPoint(k_point))+list(array_disp.GetTuple(k_point))]) + "\n")

          nodes_displacement_file.write("\n")
          nodes_displacement_file.write("\n")

    nodes_displacement_file.close()
