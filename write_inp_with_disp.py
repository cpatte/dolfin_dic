import dolfin
import glob
import numpy

def write_inp_with_disp(working_folder,
                        working_basename,
                        working_ext,
                        nb_first_node_thorax_in_abq,
                        nb_nodes_thorax,
                        nodes_filename,
                        verbose = 0):

    # working_filenames = working_folder+"/"+working_basename+"-node_displacement"+"_000001."+working_ext

    working_filenames = glob.glob(working_folder+"/"+working_basename+"-node_displacement"+"-[0-9]*"+"."+working_ext)
    assert (len(working_filenames) > 0), "There is no working file ("+working_folder+"/"+working_basename+"-node_displacement"+"-[0-9]*"+"."+working_ext+"). Aborting."

    working_zfill = len(working_filenames[0].rsplit("_",1)[-1].split(".")[0])
    if (verbose): print "working_zfill = " + str(working_zfill)

    n_frames = len(working_filenames)
    if (verbose): print "n_frames = " + str(n_frames)

    coord_disp_file = open(working_filenames[0], 'r')
    inspi_inp_file = open("displ_surface_thorax.inp", 'w')
    node_file = open(nodes_filename, 'r')
    
    dict_abq = {}
    for line in node_file.readlines():
        line = line.split(',')
        if int(line[0]) <= 788:
            dict_abq[int(line[0])] = [int(float(x)) for x in line[1:]]
    
    dict_nb_node = {}
    dict_vtk = {}
    for line in coord_disp_file.readlines():
        # print line
        if line.startswith('#'):
            continue
        else:
            line = line.split()
            nb_node_vtk = int(line[0])
            coord = [int(float(x)) for x in line[1:4]]
            disp = [float(x) for x in line[4:]]
        
            dict_vtk[nb_node_vtk] = disp
        
            for nb_node_abq, coord_abq in dict_abq.items():
                if coord_abq == coord:
                    dict_nb_node[nb_node_abq] = nb_node_vtk
    
    print 'nb_nodes_abq', len(dict_abq)
    print 'nb_nodes_vtk', len(dict_vtk)
    print 'nb_nodes_commun', len(dict_nb_node)
     
    for nb_node_abq, nb_node_vtk in dict_nb_node.items():
        inspi_inp_file.write(str(nb_node_abq+nb_first_node_thorax_in_abq-1) + ", 1, 1, " + str(dict_vtk[nb_node_vtk][0]) + "\n")
        inspi_inp_file.write(str(nb_node_abq+nb_first_node_thorax_in_abq-1) + ", 2, 2, " + str(dict_vtk[nb_node_vtk][1]) + "\n")
        inspi_inp_file.write(str(nb_node_abq+nb_first_node_thorax_in_abq-1) + ", 3, 3, " + str(dict_vtk[nb_node_vtk][2]) + "\n")   